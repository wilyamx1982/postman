# Postman Automated Testing

## Concept

[Comparing ground truth with predictions using image similarity measures](https://up42.com/blog/image-similarity-measures)

[How To Measure Image Similarities in Python](https://betterprogramming.pub/how-to-measure-image-similarities-in-python-12f1cb2b7281)

## Environment Setup

1. **Python** - for image processing

	[Python and Virtual Environment Installation](https://bitbucket.org/wilyamx1982/python/src/master/pipenv-environment-setup.md)
	
	**Installation Check**
	
	```
	ar2020 % pwd
	/Users/william.rena/.vitality/ar2020
		
	ar2020 % pipenv --where
	/Users/william.rena/.vitality/AR2020
	
	ar2020 % pipenv --version
	pipenv, version 2023.6.18
		
	ar2020 % pipenv --py
	/Users/william.rena/.local/share/virtualenvs/AR2020-76hsswaK/bin/python
	```
	
	```
	ar2020 % pipenv shell
	Launching subshell in virtual environment...
	 . /Users/william.rena/.local/share/virtualenvs/AR2020-76hsswaK/bin/activate
	william.rena@AMAT9P4KJPK2X ar2020 %  . /Users/william.rena/.local/share/virtualenvs/AR2020-76hsswaK/bin/activate
	
	(AR2020) ar2020 % python
	Python 3.11.4 (v3.11.4:d2340ef257, Jun  6 2023, 19:15:51) [Clang 13.0.0 (clang-1300.0.29.30)] on darwin
	Type "help", "copyright", "credits" or "license" for more information.
	>>> exit()
	
	(AR2020) ar2020 % exit
	
	Saving session...
	...copying shared history...
	...saving history...truncating history files...
	...completed.
	
	ar2020 % 
	```
	
1. List installed **NPM Libraries**

	[Python Shell](https://www.npmjs.com/package/python-shell) - A simple way to **run Python scripts from Node.js** with basic but efficient inter-process communication and better error handling.
	
	[ShellJS](https://www.npmjs.com/package/shelljs) - Unix shell commands
	
		npm install python-shell
		npm install shelljs
		
		~ % npm ls
		AR2020@ /Users/william.rena/.vitality/AR2020
		├── chalk@4.1.2
		├── commander@11.0.0
		├── newman@5.3.2 extraneous -> ./../../../../usr/local/lib/node_modules/newman
		├── python-shell@5.0.0
		└── shelljs@0.8.5
		
1. [image-similarity-measures 0.3.6](https://pypi.org/project/image-similarity-measures/) - Python Package. **Evaluate the similarity between two images** with eight evaluation metrics

		ar2020 % pipenv install -r ~/.vitality/ar2020/python/requirements.txt
		ar2020 % pipenv graph

	Expected **Pipfile**

		[[source]]
		url = "https://pypi.org/simple"
		verify_ssl = true
		name = "pypi"
		
		[packages]
		image-similarity-measures = "==0.3.6"
		
		[dev-packages]
		
		[requires]
		python_version = "3.11"
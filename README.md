# Postman API Testing (edit)

## References

* [Learning Postman Script](https://learning.getpostman.com/docs/postman/scripts/test_examples/)
* [Postman Dynamic Variables](https://learning.getpostman.com/docs/postman/scripts/postman_sandbox_api_reference/#dynamic-variables)
* [Postman Labs Newmann](https://github.com/postmanlabs/newman)
* [Collections](https://api.getpostman.com/collections/?apikey=PMAK-5da68009de7ca8003021a087-bc7ee2e75f73fea358da70b70d544e85ee)
* [Environments](https://api.getpostman.com/environments/?apikey=PMAK-5da68009de7ca8003021a087-bc7ee2e75f73fea358da70b70d544e85ee)

## Asserting Response Body

1. Status code

		pm.test("Media Users listing.", function() {
			pm.response.to.have.status(200);
		});

1. Get all list item

		var data = JSON.parse(responseBody);
		
		pm.test("Media users count", function() {
			pm.expect(data.length).to.be.above(0);
			
			var users = [];
		   	for (i=0; i<data.length; i++)
		   		users.push(data[i].id);
		        
		   	pm.environment.set("M-USERS", users);
		});
    
1.  Check of property

		var data = JSON.parse(responseBody);

		if (data.quiz_result) {
		    pm.test("Daily Challenge Quiz Result", function() {
		    	pm.expect(data).to.have.property('quiz_result');
		    	
		    	pm.test("Daily Challenge Quiz", function() {
		            pm.expect(data.daily_challenge_quiz).to.equal(pm.globals.get('dcq'));
		    	});
		        	
		    	pm.globals.set("dcq_points", data.quiz_result.points);
		    });
		}
		
1. Check for value

		var has_read_notification = false;
		
		pm.test("Change to notification read status", function() {
			pm.expect(has_read_notification).to.equal(true);
		});
		
1. Inspect for required values

		var data = JSON.parse(responseBody);

		var inactive_users = [];
		pm.test("Media users count", function() {
			pm.expect(data.length).to.be.above(0);
			
		    for (i=0; i<data.length; i++) {
		        var user_info = data[i];
		        if (user_info.active === false)
		            inactive_users.push(user_info.id);
		    }
		});
		
		var media_users = pm.environment.get('M-USERS');
		tests["Inactive media users " + inactive_users.length + " of " + data.length] = inactive_users.length === 0;
		
		if (inactive_users.length > 0)
		    postman.setNextRequest(null);
# Postman Automated Testing

## Dependencies

1. [Postman](https://www.postman.com/downloads/) - API platform

	[Collections](https://learning.postman.com/docs/collections/collections-overview/) - group of saved requests
	
	[Environments](https://learning.postman.com/docs/sending-requests/managing-environments/) - set of variables you can use in your Postman requests

1. [NodeJS](https://nodejs.org/en/) - javascript runtime
	
		$ % which npm
		/usr/local/bin/npm
		
		$ npm -v
		9.3.1
		
		$ % which node
		/usr/local/bin/node
		
		$ node -v
		v19.5.0
		
1. [Chalk](https://www.npmjs.com/package/chalk/v/4.1.2) - npm libray. Terminal string styling
		
		$ npm i chalk@4.1.2
		$ npm uninstall chalk

1. [Commander](https://www.npmjs.com/package/commander) - npm library. command-line interfaces.

	[Pass command line arguments](https://stackoverflow.com/questions/4351521/how-do-i-pass-command-line-arguments-to-a-node-js-program)
	
		$ npm install commander

1. [Newman](https://github.com/postmanlabs/newman) - npm library. Is a **command-line collection runner** for Postman. It allows you to effortlessly run and test a Postman collection directly from the command-line.

	[Running collections on the command line](https://learning.postman.com/docs/collections/using-newman-cli/command-line-integration-with-newman/)

		$ npm install -g newman

		$ which newman
		/usr/local/bin/newman
		
		$ newman -v
		5.3.2
		
		$ npm link newman

1. List installed **NPM Libraries**

		~ % npm ls
		AR2020@ /Users/william.rena/.vitality/AR2020
		├── chalk@4.1.2
		├── commander@11.0.0
		├── newman@5.3.2 extraneous -> ./../../../../usr/local/lib/node_modules/newman
		└── python-shell@5.0.0
		
## Setup the Scripting Files

Create a **root directory** `~/.vitality/ar2020`

	mkdir ~/.vitality

Put all the [scripts](https://discovery365-my.sharepoint.com/:f:/r/personal/p02williamr_discovery_co_za/Documents/Vitality%20Tools/AR2020?csf=1&web=1&e=Zl91xo) inside the sub-directory **ar2020**

This will create a `node_modules` directory inside the ar2020 folder above. 

	$ cd ~/.vitality/ar2020
	$ npm link newman
	
## Troubleshooting

1. What to do if all the response statuses of the image downloads were failed?

	**REGENERATE the tokens**, persist all and **EXPORT the market Environment** via Postman. Replace the file in the root directory.
	
1. Error: Cannot find module **newman**.

		$ sudo npm link newman